<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_model extends CI_Model
{	
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	public function insert($values)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		$this->db->insert("transactions", $values);
		return $this->db->insert_id();
	}

	public function insertItem($values)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		$this->db->insert("items", $values);
		return $this->db->insert_id();
	}

	public function truncate()
	{
		$this->db->from('items');
		$this->db->truncate();

		$this->db->from('transactions');
		$this->db->truncate();
	}
	
}