<?php

/* 
 input iso date : 2017-06-11T04:30:09Z
 return db date : 2017-06-11 04:30:09
*/
function iso2dbdatetime($date){
	$time = strtotime($date);
	return  date('Y-m-d H:i:s',$time);
}

/* 
 input iso datetime : 2017-06-11T04:30:09Z
 return db date : 2017-06-11
*/
function iso2dbdate($date){
	$time = strtotime($date);
	return  date('Y-m-d',$time);
}

/*
* Input : yyyy/mm/dd
* return : yyyy-mm-dd
*/
function dbdate($date,$delimiter="/"){
    list($year,$month,$date) = explode($delimiter,$date);
    
    return $year.'-'.$month.'-'.$date;
}

/*
* Input : dd/mm/yyyy
* return : yyyy-mm-dd
*/
function dbdate2($date,$delimiter="/"){
    list($date,$month,$year) = explode($delimiter,$date);
    
    return $year.'-'.$month.'-'.$date;
}

/*
* Input : yyyy-mm-dd
* return : dd/mm/yyyy
*/
function iddate($date,$delimiter="-"){
     list($year,$month,$date) = explode($delimiter,$date);
    
    return $date."/".$month."/".$year;
}

function date_indo($date){

$datefmt = new DateTime($date);
return $datefmt->format('d/m/Y');

}

function dategte($date){
	$date1 = new DateTime("now");
	$date2 = new DateTime($date);
	
	if($date2 >= $date1)
		return true;
	else
		return false;

}

function datelt($date){
	$date1 = new DateTime("now");
	$date2 = new DateTime($date);
	
	if($date2 < $date1)
		return true;
	else
		return false;
}

