<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model
{	
	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	public function insert($values)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		$this->db->insert("accounts", $values);
		return $this->db->insert_id();
	}

	public function truncate()
	{
		$this->db->from('accounts');
		$this->db->truncate();
	}
	
	public function getAccounts()
	{
		$result = array();

		$this->db->order_by('id','asc');		
		$query = $this->db->get('accounts');
		$result = $query->result();
		
		if(count($result)){
			foreach ($result as $r) {
				# code...
				$result[$r->account_id] = $r->name;
				if(isset($r->code) && $r->code){
					$result[$r->code]['Name'] = $r->name;
					$result[$r->code]['Type'] = $r->type;
				}
			}
		}

		return $result;
	}

}