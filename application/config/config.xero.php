<?php

$config = [

    'xero' => [
        // API versions can be overridden if necessary for some reason.
        //'core_version'     => '2.0',
        //'payroll_version'  => '1.0',
        //'file_version'     => '1.0'
    ],

    'oauth' => [
        'callback'    => 'oob',

        'consumer_key'      => 'TMZ4ROH6ZGXJUGEG9Z9TSUEJEZ9YDY',
        'consumer_secret'   => 's',

        //If you have issues passing the Authorization header, you can set it to append to the query string
        //'signature_location'    => \XeroPHP\Remote\OAuth\Client::SIGN_LOCATION_QUERY


        //For certs on disk or a string - allows anything that is valid with openssl_pkey_get_(private|public)
        'rsa_private_key'  => '/home/dedy/www/pdb/private.pem'
    ],
];
