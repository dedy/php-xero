<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xero extends CI_Controller {
	var $xero;

	public function __construct() {
		global $xero;
		
		parent::__construct();

		$config['xero'] = config_item('xero');
		$config['oauth'] = config_item('oauth');
		$config = [
		    'oauth' => [
		        'callback'         => 'http://localhost/',
		        'consumer_key'     => 'TMZ4ROH6ZGXJUGEG9Z9TSUEJEZ9YDY',
		        'consumer_secret'  => 'TMZ4ROH6ZGXJUGEG9Z9TSUEJEZ9YDY',
		        'rsa_private_key'  =>  config_item('private_pem_file')
		    ],
		];

		$xero = new \XeroPHP\Application\PrivateApplication($config);
		$this->load->model('account_model');
		$this->load->model('transaction_model');
		$this->load->helper('date');
	}	

	function index(){
		$this->getBankTransaction();
	}

	public function getBankTransactions(){	
		$xero  = $this->xero;

		try {
			//[BankTransactions:Read]
			$banktransaction = $xero->load('Accounting\\BankTransaction')->execute();
			//[/BankTransactions:Read]
			$str = $str . "Get all BankTransaction Total: " . count($banktransaction) . "<br>";
			$banktransaction = $xero->load('Accounting\\BankTransaction')->where('
				    Status=="' . \XeroPHP\Models\Accounting\BankTransaction::BANK_TRANSACTION_STATUS_AUTHORISED . '" AND
				    Type=="' . \XeroPHP\Models\Accounting\BankTransaction::TYPE_RECEIVE . '"
				')->execute();

			if (count($where)) {
				$str = $str . "Get an BankTransaction where Type is Receive Money: $" . $where[0]["Total"] . " on " . date_format($where[0]["Date"], 'Y-m-d') . "<br>";
			} else {
				$str = $str . "No BankTransaction of Type Recieve Money found";					
			}
		} catch (Exception $e) {
    			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}

	public function getBankTransaction(){	
		global $xero;

		//get Account Object
		$accountData = $this->account_model->getAccounts();
		$row = 0;				
				
		try {
			$page = 1;
			$this->transaction_model->truncate();

			do{
				echo "Start ".date("H:i:s").";page(".$page.")\n";
				/*$data = $xero->load('Accounting\\BankTransaction')->page($page)->where('
			    Status=="' . \XeroPHP\Models\Accounting\BankTransaction::BANK_TRANSACTION_STATUS_AUTHORISED . '" AND Date >= DateTime(2016, 07, 01)')->orderBy('Date','asc')->execute();
				*/
				$data = $xero->load('Accounting\\BankTransaction')->page($page)->where('Date >= DateTime(2016, 07, 01)')->orderBy('Date','asc')->execute();
				
				$total_data = count($data);

				if($total_data >0){
					//print_r($data[0]);
					foreach($data as $d){
						$row++;
						unset($values);
						$values['Date']  		= date_format($d["Date"], 'Y-m-d');
						$values['BankTransactionID']  		= $d['BankTransactionID'];
						$values['Status'] 		= $d['Status'];
						$values['Reference']  = $d['Reference'];
						$values['Total']  		= $d['Total'];
						$values['Type']  		= $d['Type'];
						$values['BankAccountId']  		= $d['BankAccount']['AccountID'];
						$values['BankAccountName']  		= $accountData[$d['BankAccount']['AccountID']];
						$values['CurrencyCode']  		= $d['CurrencyCode'];
						$values['response']  	= json_encode($d);
					
						$id = $this->transaction_model->insert($values);
						echo $row.". ".$d['BankTransactionID'].';'.date_format($d["Date"], 'Y-m-d').";".$d['Total']."\n";

						if(count($d['LineItems'])){
							foreach($d['LineItems'] as $item){
								unset($values);
								$values['transaction_id']  		= $id ;
								$values['BankTransactionID']  	= $d['BankTransactionID'];
								$values['LineItemID'] 			= $item['LineItemID'];
								$values['Description']  		= $item['Description'];
								$values['LineAmount']  			= $item['LineAmount'];
								if($item['AccountCode']){
									$values['AccountCode']  		= $item['AccountCode'];
									$values['AccountName']  		= isset($accountData[$item['AccountCode']])?$accountData[$item['AccountCode']]['Name']:NULL;
									$values['AccountType']  		= isset($accountData[$item['AccountCode']])?$accountData[$item['AccountCode']]['Type']:NULL;
								}

								if(isset($item['Tracking']) && $item['Tracking'][0]['TrackingCategoryID']){
									$values['TrackingOption']  		= $item['Tracking'][0]['Option'];
									$values['TrackingCategoryID']  	= $item['Tracking'][0]['TrackingCategoryID'];	
									$values['TrackingName']  	= $item['Tracking'][0]['Name'];	
								}
								$values['response']  	= json_encode($item);
								$this->transaction_model->insertItem($values);
							} //end loop lineItems
						} //end if count(lineItems)
					} //end loop data
					echo "Finish ".date("H:i:s")."\n";
				} //end if count(data)			
				$page++;
			} //end do
			while ($total_data > 0 );

		} catch (Exception $e) {
    			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}

	public function getAccounts(){	
		global $xero;
		try {
			$data = $xero->load('Accounting\\Account')->where('
			    Status=="' . \XeroPHP\Models\Accounting\Account::ACCOUNT_STATUS_ACTIVE . '"')->execute();

			if(count($data)){
				//truncate the table 
				$this->account_model->truncate();
				foreach($data as $d){
					$values['account_id']  	= $d['AccountID'];
					$values['code']  		= $d['Code'];
					$values['name'] 		= $d['Name'];
					$values['description']  = $d['Description'];
					$values['type']  		= $d['Type'];
					$values['currency']  	= $d['CurrencyCode'];
					$values['status']  		= $d['Status'];
	
					$this->account_model->insert($values);

				}
			}
		} catch (Exception $e) {
    			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}

}
